package com.itu.dsm.client.shell;

public interface ICommand {
    boolean execute();
    String name();
    void set_param(String param);
}
