package com.itu.dsm.client.shell;

import com.dsm.itu.rpc.ThroughputResult;
import com.itu.dsm.client.StateMachineClient;

public class GetResults implements ICommand {

    private String param;
    private String name;
    private StateMachineClient client;

    public GetResults(String name, StateMachineClient client) {
        super();
        this.name = name;
        this.client = client;
    }

    @Override
    public boolean execute() {
        ThroughputResult result = this.client.getResults();

        if (result != null) {
            double th = ((double)result.getNumberOfRequest() / ((double)result.getElapsedTimeInMilli() / 1000.0) * 60.0);
            System.out.println(
                "NumberOfRequest: " + result.getNumberOfRequest() + " Elapsed: " + result.getElapsedTimeInMilli()
                        + " Througput: " + th + " req/min");
            return true;
        }

        System.err.println("No result is arrived");        

        return false;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void set_param(String param) {
        this.param = param;
    }

}
