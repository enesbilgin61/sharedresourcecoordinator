# Coordinated Access to Shared Resources for Geo-replicatedState Machines

## ENVIROMENT
- Java
- GRPC
- Spring State Machines
- Zookeeper
- Docker

## CLIENT
- Client can work in closed loop / or can be managed by command line.
- Need some start-up arguments to be passed before its run like address of server and its port.
- Please check the client code to see available commands.

## BUILD
- Subprojects include Maven dependency files (pom.xml) which will download all neccessary libraries for you.
- Run `mvn clean install` to create packages. First build RPC subproject then the others.
- Run `mvn clean compile assembly:single` to create Client and Server packages with their dependencies.
- `java -jar xxxx.jar` command can be used to execute generated jar files for Client and Server.


## DEPLOYMENT
- `<Workspace>\Server\VM-Conf` directory includes docker-compose files and other authentication files to deploy machines on server 
- Each server machine requires a config file to know addresses of resources and other servers. Structure of a config file is easy to understand. To properly execute to all system, each config must be matched.
- Server machine are named with their geographical locations.
- Remote server machines need Docker to be installed
- Our server side docker image can be accessed from [Docker HUB](https://hub.docker.com/repository/docker/acrobat61/sm-server) which also used in our compose files.
- `config.json` and `docker-compose.yml` must be transfered to servers to run the Server Side Application.
- `sudo docker-compose up -d --build` can be used to run server application.