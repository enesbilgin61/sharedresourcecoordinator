#!/bin/bash

echo "Transfering to Asia"
cd asia;
scp -i smoc-asia.pem config.json docker-compose.yml ubuntu@ec2-65-0-85-20.ap-south-1.compute.amazonaws.com:~/

echo "Transfering to Asia2"
cd ../asia2;
scp -i smoc-asia.pem config.json docker-compose.yml ubuntu@ec2-65-0-122-161.ap-south-1.compute.amazonaws.com:~/
scp -i smoc-asia.pem ../../../Client/client/target/client-1.0-SNAPSHOT-jar-with-dependencies.jar ubuntu@ec2-65-0-122-161.ap-south-1.compute.amazonaws.com:~/

echo "Transfering to US"
cd ../us;
scp -i smoc-us.pem config.json docker-compose.yml ubuntu@ec2-3-216-125-200.compute-1.amazonaws.com:~/

echo "Transfering to US2"
cd ../us2;
scp -i smoc-us.pem config.json docker-compose.yml ubuntu@ec2-54-152-212-141.compute-1.amazonaws.com:~/
scp -i smoc-us.pem ../../../Client/client/target/client-1.0-SNAPSHOT-jar-with-dependencies.jar ubuntu@ec2-54-152-212-141.compute-1.amazonaws.com:~/

echo "Transfering to UK"
cd ../uk;
scp -i smoc-eu.pem config.json docker-compose.yml ubuntu@ec2-52-215-48-244.eu-west-1.compute.amazonaws.com:~/
scp -i smoc-eu.pem ../../../Client/client/target/client-1.0-SNAPSHOT-jar-with-dependencies.jar ubuntu@ec2-52-208-110-243.eu-west-1.compute.amazonaws.com:~/

echo "Transfering to UK2"
cd ../uk2;
scp -i smoc-eu.pem config.json docker-compose.yml ubuntu@ec2-52-208-110-243.eu-west-1.compute.amazonaws.com:~/

echo "Transfering to Sydney"
cd ../sydney;
scp -i smoc-aus.pem config.json docker-compose.yml ubuntu@ec2-52-63-197-242.ap-southeast-2.compute.amazonaws.com:~/

echo "Transfering to Brazil"
cd ../brazil;
scp -i smoc-amer.pem config.json docker-compose.yml ubuntu@ec2-15-228-100-124.sa-east-1.compute.amazonaws.com:~/
scp -i smoc-amer.pem ../../../Client/client/target/client-1.0-SNAPSHOT-jar-with-dependencies.jar ubuntu@ec2-15-228-100-124.sa-east-1.compute.amazonaws.com:~/