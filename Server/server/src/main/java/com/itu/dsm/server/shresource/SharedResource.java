package com.itu.dsm.server.shresource;

import com.itu.dsm.server.config.SharedResourceConf;
import com.itu.dsm.server.queue.DistributedQueue;

public class SharedResource {
    
    public DistributedQueue queue;

    public SharedResourceConf conf;

    public SharedResource(SharedResourceConf conf, DistributedQueue queue) {
        super();
        this.conf = conf;
        this.queue = queue;
    }

    public SharedResource(SharedResourceConf conf) {
        super();
        this.conf = conf;
    }

}
