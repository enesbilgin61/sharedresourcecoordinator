package com.itu.dsm.server.queue;

public interface IElement {

	byte[] toByte();

	void fromByte(byte[] take);
}
