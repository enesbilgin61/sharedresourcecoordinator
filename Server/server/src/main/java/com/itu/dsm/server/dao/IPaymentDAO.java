package com.itu.dsm.server.dao;

import com.itu.dsm.server.models.Payment;

public interface IPaymentDAO {
    void add(Payment entry);
}
