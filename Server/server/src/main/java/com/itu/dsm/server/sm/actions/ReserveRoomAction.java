package com.itu.dsm.server.sm.actions;

import com.itu.dsm.server.config.ClusterConfig;
import com.itu.dsm.server.models.RoomReservation;
import com.itu.dsm.server.shresource.SharedResource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;

public class ReserveRoomAction implements Action<String, String> {

    private final Log log = LogFactory.getLog(ReserveRoomAction.class);
    private ClusterConfig conf;
    private SharedResource resource;
    private MongoClient mongoClient;
    private MongoDatabase database;
    private MongoCollection<RoomReservation> collection;

    public ReserveRoomAction(ClusterConfig conf, SharedResource resource) {
        super();
        this.conf = conf;
        this.resource = resource;

        if (conf.me.equals(resource.conf.first_to_access)) {

            CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                    fromProviders(PojoCodecProvider.builder().automatic(true).build()));

            MongoClientSettings settings = MongoClientSettings.builder().codecRegistry(pojoCodecRegistry)
                    .applyConnectionString(
                            new ConnectionString("mongodb://" + resource.conf.ip_address + ':' + resource.conf.port))
                    .build();

            log.info("This device owner of shared resource so it created the database");
            System.out.println("This device owner of shared resource so it created the database");

            mongoClient = MongoClients.create(settings);

            database = mongoClient.getDatabase("reservations");

            collection = database.getCollection("reservations", RoomReservation.class);
            //collection.drop();
            consume_previous_messages();
        }
    }

    @Override
    public void execute(StateContext<String, String> context) {
        if (conf.me.equals(conf.leader) && conf.me.equals(resource.conf.first_to_access)) {
            RoomReservation model = new RoomReservation();
            model.room_id = 14;
            try {
                resource.queue.push(model);
                collection.insertOne(model);
                resource.queue.pop(model);
            } catch (Exception e) {
                log.error("Leader can not proceed the room reservation");
                System.err.println("Leader can not proceed the room reservation");
            }
            log.info("This device is leader and also owner of shared resource");
            System.out.println("This device is leader and also owner of shared resource");
        } else if (conf.me.equals(resource.conf.first_to_access)) {
            RoomReservation model = new RoomReservation();
            try {
                resource.queue.pop(model);
                collection.insertOne(model);
                log.info("Owner of Shared resource process the room reservation");
                System.out.println("Owner of Shared resource process the room reservation");
            } catch (Exception e) {
                log.error("SharedResource owner can not pop the room reservation");
                System.err.println("SharedResource owner can not pop the room reservation");
            }
        } else if (conf.me.equals(conf.leader)) {
            RoomReservation model = new RoomReservation();
            model.room_id = 14;
            try {
                resource.queue.push(model);
                log.info("Leader delegate the room reservation to near replica");
                System.out.println("Leader delegate the room reservation to near replica");
            } catch (Exception e) {
                log.error("Leader can not push the room reservation");
                System.err.println("Leader can not push the room reservation");
            }
        }

        log.info("Room is reserved");
        System.out.println("Room is reserved");
    }

    private void consume_previous_messages() {
        RoomReservation model = new RoomReservation();
        while (resource.queue.poll(model)) {
            collection.insertOne(model);
        }
    }
}
