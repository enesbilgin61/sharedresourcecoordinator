package com.itu.dsm.server.sm;

public enum Events {
    MAKE_RESERVATION,
    PAY_MONEY,
    TAKE_ROOM,
    CANCEL
}
