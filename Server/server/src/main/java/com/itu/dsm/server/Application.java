package com.itu.dsm.server;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itu.dsm.server.communication.RPCServer;
import com.itu.dsm.server.config.ClusterConfig;
import com.itu.dsm.server.sm.StateMachineConfig;

import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.statemachine.StateMachine;

public class Application {

	public static void main(String[] args) throws Exception {
		int port = 8980;
		if (args.length > 0) {
			port = Integer.parseInt(args[0]);
		}

		LogManager.getRootLogger().setLevel(Level.ALL);

		ObjectMapper mapper = new ObjectMapper();
		
		StateMachineConfig.conf = mapper.readValue(new File("config.json"), ClusterConfig.class);

		System.out.println(StateMachineConfig.conf.me.toString());

		StateMachine<String, String> machine = new AnnotationConfigApplicationContext(StateMachineConfig.class).getBean(StateMachine.class);
		machine.start();

		RPCServer server = new RPCServer(port, machine);
		server.start();

		server.blockUntilShutdown();

	}
}