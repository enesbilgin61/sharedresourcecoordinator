package com.itu.dsm.server.sm;

public enum States {
    INITIAL, 
    RESERVATION_REACHED,
    PAID,
    RESERVED,
    CANCELED
}